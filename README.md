## Data-driven Discovery of Mathematical and Physical Relations in Oncology Data using Human-understandable Machine Learning

Code repository for 

"Data-driven Discovery of Mathematical and Physical Relations in Oncology Data using Human-understandable Machine Learning"  

by Daria Kurz, Carlos Salort Sánchez, and Cristian Axenie 

submitted to Frontiers in AI - Research Topic: Advances in Mathematical and Computational Oncology, Volume II.

Codebase:

datasets - the experimental datasets (csv files) and their source, each in separate directories

models   - codebase to run and reproduce the experiments for the different data-driven learning systems

Each of the models has specific scripts to run individual systems and generate graphs.
