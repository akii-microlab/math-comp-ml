import time
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import torch
import torch.nn as nn
import torch.optim as optim
from sklearn import svm
from sklearn.metrics import mean_squared_error
from torch.utils.data import Dataset, DataLoader


def aux_prepare_data(df: pd.DataFrame):
    n = df.shape[0]
    x = []
    x_o = []
    y = []
    for it in range(n - 1):
        x.append(df.iloc[:it + 1][['Time', 'Observation']].to_numpy())
        x_o.append(df.iloc[it + 1][['Time']].to_numpy())
        y.append(df.iloc[it + 1][['Observation']].to_numpy())
    return x, x_o, y


def prep_data(df: pd.DataFrame):
    x = []
    x_o = []
    y = []
    for it in df.ID.unique():
        temp_x, temp_x_o, temp_y = aux_prepare_data(df.loc[df.ID == it])
        x += temp_x
        x_o += temp_x_o
        y += temp_y
    return [x, x_o], y


def normalize(df: pd.DataFrame):
    m = df[['Time', 'Observation']].max()
    df[['Time', 'Observation']] /= m
    return df, m


class Data(Dataset):
    def __init__(self, data_x, data_y):
        self.data_x = [[torch.Tensor(j) for j in i] for i in data_x]
        self.data_y = [torch.Tensor(i) for i in data_y]

    def __len__(self):
        return len(self.data_y)

    def __getitem__(self, item: int):
        return [self.data_x[0][item], self.data_x[1][item]], self.data_y[item]


class Model(nn.Module):
    def __init__(self, feature_size: int, hidden_size: int = 128):
        super(Model, self).__init__()
        self.feature_size = feature_size
        self.hidden_size = hidden_size
        self.gru = nn.GRU(input_size=feature_size, hidden_size=hidden_size,
                          batch_first=True)
        self.linear = nn.Linear(in_features=hidden_size + 1, out_features=1)

    def forward(self, x: list):
        data = x[0]
        step_out = x[1]
        gru_out, _ = self.gru(data)
        gru_out = torch.relu(gru_out[:, -1, :])
        c = torch.cat([gru_out, step_out], dim=1)
        out = self.linear(c)
        return torch.relu(out)


def train_test_svm(train_data, test_data):
    X_train = train_data.Time.to_numpy().reshape(-1, 1)
    y_train = train_data.Observation.to_numpy().ravel()

    X_test = test_data.Time.to_numpy().reshape(-1, 1)
    y_test = test_data.Observation.to_numpy().ravel()

    regr = svm.SVR()
    regr.fit(X_train, y_train)
    pr = regr.predict(X_test)
    mse = mean_squared_error(y_test, pr)
    # print(f'SVM test MSE: {mse}')
    return regr, mse


def format_dataset_2(dt):
    final_dt = pd.DataFrame()
    for i in range(int(dt.shape[1] / 2)):
        temp_dt = dt.iloc[:, 2*i:2*(i + 1)]
        temp_dt.columns = ['Time', 'Observation']
        temp_dt = temp_dt.dropna()
        temp_dt['ID'] = int(i)
        final_dt = final_dt.append(temp_dt)
    return final_dt


def main():
    seed = 1993
    torch.manual_seed(seed)
    np.random.seed(seed)
    datasets = [
        'datasets/0/',
        'datasets/1/MDA-MB-231dTomato.csv',
        'datasets/2/angio-genesis.csv',
        'datasets/3/LM2-4LUC.csv']
    # path = 'datasets/3/LM2-4LUC.csv'
    minibatch = 1
    feature_size = 2
    epochs = 100
    min_n_augments = 10
    augmentation_percentage = .5
    n_cv_sets = 4
    # noise = True
    results = {}
    for path in datasets:
        print(path)
        results[path] = {'svm': [],
                         'dl_no_augmentation': [],
                         'dl_augmentation': [],
                         'dl_augmentation_noise': [],
                         'samples': []}
        if path == 'datasets/0/':
            dt = read_dataset_0(path)
        else:
            dt = pd.read_csv(path)
            if path == 'datasets/2/angio-genesis.csv':
                dt = format_dataset_2(dt)
        dt, normalization = normalize(dt)
        if path == 'datasets/0/':
            # This dataset only has two observations, so we will try to learn on one and predict on the second one
            cv_sets = np.array_split(dt.ID.unique(), 2)
        else:
            cv_sets = np.array_split(dt.ID.unique(), n_cv_sets)
        for samples_to_test in cv_sets:
            results[path]['samples'].append(list(samples_to_test))
            dt_test = dt[dt.ID.isin(samples_to_test)]
            dt_train = dt[~dt.ID.isin(samples_to_test)]
            n_augments = max(min_n_augments, int(len(dt_train.ID.unique()) * augmentation_percentage))
            svm, mse_svm = train_test_svm(dt_train, dt_test)
            print('No augmentation')
            losses_no_augment = train_test_dl(
                dt_test, dt_train, epochs, feature_size, minibatch,
                n_augments=0, noise=False, svm=svm)
            print('Augmentation')
            losses_augment = train_test_dl(
                dt_test, dt_train, epochs, feature_size, minibatch,
                n_augments=n_augments, noise=False, svm=svm)
            print('Augmentation + noise')
            losses_augment_noise = train_test_dl(
                dt_test, dt_train, epochs, feature_size, minibatch,
                n_augments=n_augments, noise=True, svm=svm)
            print(f'SVM test MSE: {mse_svm}')
            print(f'No augment MSE: {np.mean(losses_no_augment)}')
            print(f'Augment MSE: {np.mean(losses_augment)}')
            print(f'Augment + noise MSE: {np.mean(losses_augment_noise)}')
            results[path]['svm'].append(mse_svm)
            results[path]['dl_no_augmentation'].append(np.mean(losses_no_augment))
            results[path]['dl_augmentation'].append(np.mean(losses_augment))
            results[path]['dl_augmentation_noise'].append(np.mean(losses_augment_noise))
            # outputs_unnormalized = np.array(outputs) * normalization[1]
            # dt_test_unnormalized = dt_test.copy()
            # dt_test_unnormalized = dt_test_unnormalized.iloc[1:]
            # dt_test_unnormalized[['Time', 'Observation']] *= normalization
            # plt.figure()
            # plt.title(sample_to_test)
            # plt.plot(dt_test_unnormalized['Time'],
            #          dt_test_unnormalized['Observation'])
            # plt.plot(dt_test_unnormalized['Time'], outputs_unnormalized)
            # plt.legend(['Original values', 'Predicted'])
        # plt.show()
    pd.DataFrame(results).to_csv('results.csv')


def read_dataset_0(path):
    temp_dt_1 = pd.read_csv(path + 'Paclitaxel_10nm_cell_conc.csv')
    temp_dt_1.columns = ['Time', 'Observation']
    temp_dt_1['ID'] = 0
    temp_dt_2 = pd.read_csv(path + 'Paclitaxel_10nm_medium_conc.csv')
    temp_dt_2.columns = ['Time', 'Observation']
    temp_dt_2['ID'] = 1
    dt = temp_dt_1.append(temp_dt_2)
    return dt


def train_test_dl(dt_test, dt_train, epochs, feature_size,
                  minibatch, n_augments, noise, svm):
    if n_augments > 0:
        dt_augmented = augment_data(svm, dt_train, n_augments, noise)
        dt_train = pd.concat([dt_train, dt_augmented])
    train_x, train_y = prep_data(dt_train)
    train_data = Data(train_x, train_y)
    train_loader = DataLoader(train_data, batch_size=minibatch)
    test_x, test_y = prep_data(dt_test)
    test_data = Data(test_x, test_y)
    test_loader = DataLoader(test_data, batch_size=minibatch)
    model = Model(feature_size)
    model.train()
    criterion = nn.MSELoss()
    optimizer = optim.SGD(model.parameters(), lr=.001, momentum=.9)
    train_losses = []
    time_needed = []
    for epoch in range(epochs):
        start = time.time()
        epoch_loss = []
        for x, y in train_loader:
            optimizer.zero_grad()
            output = model(x)
            loss = criterion(output, y)
            loss.backward()
            epoch_loss.append(loss.item())
            optimizer.step()
        if epoch % 20 == 0:
            print(f'Epoch {epoch};\tTime needed: {time.time() - start};'
                  f'\tTrain loss: {np.mean(epoch_loss)}')
        train_losses.append(np.mean(epoch_loss))
        time_needed.append(time.time() - start)
    # print('Start test')
    model.eval()
    outputs = []
    ys = []
    losses = []
    for x, y in test_loader:
        output = model(x)
        loss = criterion(output, y)
        outputs.append(output)
        ys.append(y)
        losses.append(loss.item())
    return losses


def augment_data(model, dt, n, noise=False):
    new_dts = []
    for i in range(n):
        temp_id = 100+i
        times = np.sort(dt.Time.sample(8).unique())
        observation = model.predict(times.reshape(-1, 1))
        if noise:
            observation += np.random.normal(loc=0, scale=.01,
                                            size=observation.shape)
        temp_dt = pd.DataFrame({'ID': temp_id, 'Time': times,
                                'Observation': observation})
        new_dts.append(temp_dt)
    return pd.concat(new_dts)


if __name__ == '__main__':
    main()
