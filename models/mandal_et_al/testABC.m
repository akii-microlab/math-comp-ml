% set up environment
clear all; close all; clc;
% sample size
n=1500;
% simmetrically distributed data
drange = 1;
% choose between uniformly distributed data and non-uniform distribution
dist = 'uniform'; % {uniform, non-uniform}
% non-unifrom distribution type {power-law, Gauss dist}
nufrnd_type = 'gauss';
% use power-law relations for the moment
exponent = 3;
% generate training data
switch (dist)
    case 'uniform'
        % generate NUM_VALS random samples in the given interval
        X  = -drange + rand(n, 1)*(2*drange);
        Y = X.^exponent;
    case 'non-uniform'
        switch nufrnd_type
            case 'plaw'
                % generate NUM_VALS random samples in the given
                % interval - the function is truncated only in the
                % positive quadrant
                X = rand(n, 1)*(drange);
                X = nufrnd_plaw(X, 0.000001, drange, exponent);
                Y = X.^exponent;
            case 'gauss'
                % generate NUM_VALS random samples in the given
                % interval - the function is truncated only in the
                % positive quadrant
                X  = randn(n, 1)*(drange/3);
                Y = X.^exponent;
        end
end
% visualize the input
figure; plot(X,Y,'.r');
xlabel('X','fontsize',14);
ylabel('Y','fontsize',14);
title('Input data');
% run the algorithm
[Wx_mat, Wy_mat] = ABC(X,Y,'p',1,'Replication',5);
% visualize the output
hold on; plot(X*Wx_mat ,Y*Wy_mat,'.g','MarkerSize',2);
xlabel('X','fontsize',14);
ylabel('Y','fontsize',14);
title('AB-canonical extracted relationship')
