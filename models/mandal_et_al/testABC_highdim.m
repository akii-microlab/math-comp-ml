% tes AB divergence learning for 6-dim X and 4-dim Y vectors
close all; clc; clear all;
% sample size
n=500;
% number of variables in X and Y
px = 6; py = 6;
% Data
X = randn([n,px]); Y = randn([n,py]);

% original 
% px = 6; py=4;
% c1=rand(px,1) - .5;
% Y1 = sin(3*X*c1) + .05*randn([n,1]);
% c2=rand(px,1) - .5; c2 = c2 - ((c2'*c1)/(c1'*c1))*c1; 
% Y2 =  (X*c2).^3 - X*c2 + .05*randn([n,1]);

% simple example for nst thesis
c1=rand(px,1) - .5;
Y1 = (X*c1).^2 + .05*randn([n,1]);
c2=rand(px,1) - .5; c2 = c2 - ((c2'*c1)/(c1'*c1))*c1; 
Y2 =  (X*c2).^3 + .05*randn([n,1]);

figure; set(gcf, 'color', 'w'); box off;
subplot(2,2,1);
plot(X*c1,Y1,'.r'); box off;
xlabel('$x_1$', 'interpret','latex','fontsize',14);
ylabel('$y_1$', 'interpret','latex','fontsize',14);
subplot(2,2,2); 
plot(X*c2,Y2,'.b'); box off;
xlabel('$x_2$', 'interpret','latex','fontsize',14);
ylabel('$y_2$', 'interpret','latex','fontsize',14);

c3 = rand(2) - .5;
c3(2,:) = c3(2,:) - ((c3(2,:)*c3(1,:)')/(c3(1,:)*c3(1,:)'))*c3(1,:);
Y(:,1) =  [Y1 Y2]*c3(:,1);
Y(:,2) = [Y1 Y2]*c3(:,2);

% full simulation 
% [Wx_mat, Wy_mat, coeff, ro] = ABC(X,Y,'p',5,'Replication',10);

% short simulation 
[Wx_mat, Wy_mat] = ABC(X,Y,'p',5,'Replication',10);

subplot(2,2,4);
plot(X*Wx_mat(:,1) ,Y*Wy_mat(:,1),'.k','MarkerSize',10);box off;
hold on; f=fit(X*Wx_mat(:,1), Y*Wy_mat(:,1),'poly3'); plot(f, 'g', X*Wx_mat(:,1), Y*Wy_mat(:,1), '.k'); legend off;
xlabel('$u_1$', 'interpret','latex','fontsize',14);
ylabel('$v_1$', 'interpret','latex','fontsize',14);

subplot(2,2,3);
plot(X*Wx_mat(:,2) ,Y*Wy_mat(:,2),'.k','MarkerSize',10); box off;
hold on; f=fit(X*Wx_mat(:,2), Y*Wy_mat(:,2),'poly3'); plot(f, 'g', X*Wx_mat(:,2), Y*Wy_mat(:,2),'.k'); legend off;
xlabel('$u_2$', 'interpret','latex','fontsize',14);
ylabel('$v_2$', 'interpret','latex','fontsize',14);

% full simulation
% disp 'Canonical coefficients';
% coeff
% disp 'Correlation coefficient';
% ro
