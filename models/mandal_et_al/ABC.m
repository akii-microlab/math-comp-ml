function [Wx_mat, Wy_mat, R_AB, p_value] = ABC(X,Y,varargin)
% AB-Canonical Analysis
% 
% Inputs:
% X = (n,px) data matrix, n = number of observations, px = number of variables.
% Y = (n,py) data matrix, n = number of observations, py = number of variables.
% 
% Optional inputs:
% a = (scalar) alpha parameter of AB-divergence, default is 0.5
% b = (scalar) beta parameter of AB-divergence, default is 0.5
% p = (integer) Number of canonical variates to be extracted, default is min(px,py)
% rVal = (scalar in [0 1]) Threshold value of r (maximal correlation) for
%        convergence criteria, default is 0
% R = Number of permutation for the permutation test, default is 1000
% Replication = Number of replications (solution may stuck to local maxima,
%               so repeatation is needed), default is 10
% Reduce = Number of points where density functions will be evaluated
%          (needed for fast calcualtion in case of large sample size,
%          preferably more than 100), default is n, i.e. all samples 
% verbose = option for printing steps of the algorithm
%            options are true (default) or false.
%
% Outputs:
% Wx_mat = (px,p) matrix, columns are the canonical coefficients of X
% Wy_mat = (py,p) matrix, columns are the canonical coefficients of Y
% R_AB = p vector, the AB canonical coefficients (maximal correlation) in
%        increasing order 
% p_value = p vector, p-value of the tests where the null hypothesis null
%           AB canonical coefficients (need to modify)
%
% See also canoncorr
% _________________________________________________________________________
%                          Example
% % sample size
% n=100;
% % number of variables in X and Y
% px = 6; py=4;
% % Data
% X = randn([n,px]); Y = randn([n,py]);
% 
% c1=rand(px,1) - .5;
% Y1 = sin(3*X*c1) + .05*randn([n,1]);
% c2=rand(px,1) - .5; c2 = c2 - ((c2'*c1)/(c1'*c1))*c1; 
% Y2 =  (X*c2).^3 - X*c2 + .05*randn([n,1]);
% 
% figure; plot(X*c1,Y1,'.')
% xlabel('$w_x'' x$', 'interpret','latex','fontsize',14)
% ylabel('$w_y'' y$', 'interpret','latex','fontsize',14)
% title('First set of latent variables.')
% figure; plot(X*c2,Y2,'.')
% xlabel('$w_x'' x$', 'interpret','latex','fontsize',14)
% ylabel('$w_y'' y $', 'interpret','latex','fontsize',14)
% title('Second set of latent variables.')
% drawnow
% 
% c3 = rand(2) - .5;
% c3(2,:) = c3(2,:) - ((c3(2,:)*c3(1,:)')/(c3(1,:)*c3(1,:)'))*c3(1,:);
% Y(:,1) =  [Y1 Y2]*c3(:,1);
% Y(:,2) = [Y1 Y2]*c3(:,2);
% 
% [Wx_mat Wy_mat] = ABC(X,Y,'p',2,'Replication',10);
% 
% figure; plot(X*Wx_mat(:,1) ,Y*Wy_mat(:,1),'.','MarkerSize',10)
% xlabel('$u_1$', 'interpret','latex','fontsize',14)
% ylabel('$v_1$', 'interpret','latex','fontsize',14)
% title('First AB-canonical variate pairs.')
% 
% figure; plot(X*Wx_mat(:,2) ,Y*Wy_mat(:,2),'.','MarkerSize',10)
% xlabel('$u_2$', 'interpret','latex','fontsize',14)
% ylabel('$v_2$', 'interpret','latex','fontsize',14)
% title('Second AB-canonical variate pairs.')
% _________________________________________________________________________
%                               Comments
% 
% 1. Algorithm stops at local maxima, so it is needed to run the algorithm
% 'rep' number of times.
% 2. Algorithm is slow for large data set. So we need to choose small number
% of points where density functions will be  evaluated by setting 'reduce'
% option. However, it uses all samples to calculated density function.
%
% Created on: 21.01.2013
% Modified on: 27.06.2013
% Abhijit Mandal, BSI, RIKEN, Japan
% _________________________________________________________________________

global global_var;

[n1, px] = size(X);
[n2, py] = size(Y);
if n1~=n2, error('X and Y should have same number of observations in their rows');end

% default values of the parameters
% % divergence parameters
a=0.5; b=0.5;
% Number of canonical variates to be extracted
p = min([px,py]);
% Threshold value of r (maximal correlation) for convergence criteria
rVal = 0;
% Number of permutation for the permutation test
R = 1000;
% solution stuck to local maxima, so repeatation is needed
rep = 10;
% Density evaluation at pt points
pt = n1;
verbose = true;

% checking length of the arguments
n_argin = length(varargin);
if mod(n_argin,2) > 0
    error('Wrong arguments: must be name-value pairs.');
end

% assigning arguments
for i = 1:2:n_argin
    switch lower(varargin{i})
        case 'a'
            a=varargin{i+1};
        case 'b'
            b=varargin{i+1};
        case 'p'
            p=varargin{i+1};
        case 'rval'
            rVal=varargin{i+1};
        case 'r'
            R=varargin{i+1};
        case 'replication'
            rep=varargin{i+1};
        case 'reduce'
            pt=varargin{i+1};
        case 'verbose'
            verbose=varargin{i+1};
        otherwise
            error('invalid argument: %s', varargin{i})
    end
end

if p>min([px,py]),fprintf('\nWarning: Maximum possible canonical variable paris is %i.\n',min([px,py]));p=min([px,py]);end
if verbose, fprintf('\nAB-canonical correlation analysis, with parameters a=%g, b=%g\n',a,b);end

% storing in global variables
global_var.rep=rep; global_var.verbose=verbose;
global_var.px=px; global_var.py=py;
global_var.pt = pt; global_var.rVal = rVal;

if verbose && pt~=n1, fprintf('\nDensity evaluation at %i points.\n',pt);end
if verbose && n1>100 && pt>100, fprintf('\nLarge data: set ''reduce'' less than 100 for fast calculation.\n');end

Wx_mat = []; Wy_mat = [];
tic
for i=1:p
    
    if verbose,fprintf('\nStep %i/%i\n_______________\n\n', i, p);end
    
    WxWy = canon_variate(X,Y,Wx_mat,Wy_mat,a,b);
    
    Wx = WxWy(1:px); Wy = WxWy(px+1:end);
    Wx_mat = [Wx_mat Wx]; Wy_mat = [Wy_mat Wy]; %#ok<AGROW>
    
end

if nargout==4
    p_value = permutation_test(X,Y,Wx_mat,Wy_mat,a,b,R);
end

if verbose,time_taken=sec2hour(toc);fprintf('\nTime Taken: %s\n', time_taken);end

% The ABC coefficients (maximal correlation)
R_AB = zeros(1,p);
for i=1:p
    R_AB(i) = ace([Y*Wy_mat(:,i) X*Wx_mat(:,i)]');
%     R_AB(i) = -ABC_coeff([Wx_mat(:,i); Wy_mat(:,i)],X,Y,a,b);
end

% Permutes the canonical variates according to the ABC coefficients
[~, index] = sort(R_AB,'descend');
Wx_mat = Wx_mat(:,index); Wy_mat = Wy_mat(:,index);
R_AB = R_AB(index);

% _________________________________________________________________________
function WxWy = canon_variate(X,Y,Wx_mat,Wy_mat,a,b)
% computes the canonical variate pairs
global global_var;

rep = global_var.rep; verbose = global_var.verbose; rVal = global_var.rVal;
px = global_var.px; py = global_var.py;

options = optimoptions(@fmincon,'Algorithm','interior-point','Display','off');

r_opt = 0; % optimum value of r=maximal correlation
ABC_opt = 0; % optimum value of AB-divergence measure

for count=1:rep
      
    % initial value of Wx and Wy
    Wx = randn([px,1]);
    Wx = Wx/sqrt(Wx' * Wx);
    Wy = randn([py,1]);
    Wy = Wy/sqrt(Wy' * Wy);
    WxWy0 = [Wx; Wy];
    
    [WxWy_temp , ~, exitFlag] = fmincon(@(WxWy_var) ABC_coeff(WxWy_var,X,Y,a,b),WxWy0,[],[],[],[],[],[],...
        @(WxWy1) const_fun(WxWy1,Wx_mat,Wy_mat),options);
    
    %Algorithm fails to converge
    if verbose && exitFlag<=0, fprintf('\nAlgorithm fails to converge, exit flat = %d\n', exitFlag);end
    
    r = ace([Y*WxWy_temp(px+1:end) X*WxWy_temp(1:px)]');
    ABC_val = -ABC_coeff(WxWy_temp,X,Y,a,b);
    if verbose, fprintf('Iteration %i: r = %g, abc = %g\n',count,r, ABC_val);end
    if rVal>0
        if r > r_opt
            r_opt = r; WxWy = WxWy_temp;
        end
        if r_opt>rVal, break;end
    else
        if ABC_val > ABC_opt
            ABC_opt = ABC_val; WxWy = WxWy_temp;
        end
    end
end

if r_opt<rVal
    fprintf('Warning: Maximum no. of iterations exceeded.\n')
    fprintf('Max(r) = %g: Failed to meet r threshold of %g.\n',r_opt,rVal);
end

% _________________________________________________________________________
function val = ABC_coeff(WxWy,X,Y,a,b)
% (Negative of) AB Canonical coefficient between Wx'X and Wy'Y, where a and
% b are the divergence parameters

global global_var;
px = global_var.px; 
pt = global_var.pt; 

Wx = WxWy(1:px); Wy = WxWy(px+1:end);
U1 = X*Wx; U2 = Y*Wy;

% points where the density function will be evaluated
if length(U1)==pt
    U10 = U1; U20 = U2;
else
    U10 = (linspace(min(U1),max(U1),pt))'; U20 = (linspace(min(U2),max(U2),pt))';
end

% joint density of (U1,U2) at (U1_i,U2_i)
f_xy = ksdensity2d(U10,U20,U1,U2);
f_x = ksdensity1d(U10,U1);
f_y = ksdensity1d(U20,U2);
val = -AB_divergence(a,b,f_xy,f_x.*f_y);

% _________________________________________________________________________
function val = AB_divergence(a,b,p,q)
% the AB divergence between the densities p and q, where a and b are the
% divergence parameters

if length(p) ~= length(q)
    error('p and q must be of same length.')
end

if a~=0 && b~=0 && a+b~=0
    val = -sum( p.^a .* q.^b - (a/(a+b))*p.^(a+b) - (b/(a+b))*q.^(a+b) )/(a*b);
elseif a~=0 && b==0
    val = sum( p.^a .* log((p./q).^a) - p.^a + q.^a )/(a^2);
elseif a~=0 && b==-a
    val = sum( log((q./p).^a) + (p./q).^a -1 )/(a^2);
elseif a==0 && b~=0
    val = sum( q.^b .* log((q./p).^b) - q.^b + p.^b )/(b^2);
elseif a==0 && b==0
    val = sum(( log(p) - log(q) ).^2)/2;
else
    error('error')
end

% _________________________________________________________________________
function f = ksdensity2d(x,y,Xi,Yi)
% Computes kernel density estimate in 2D.
% (x,y): points where the density function will be evaluated
% (Xi,Yi): data

n = length(Xi);
if length(Yi)~=n, error('Xi and Yi must be of same length');end

n1 = length(x);
if length(y)~=n1, error('x and y must be of same length');end


% Choose bandwidths for Gaussian kernel
h1 = std(Xi) * (1/n)^(1/6);
h2 = std(Yi) * (1/n)^(1/6);

mu1(1,:) = Xi; mu1 = repmat(mu1,[n1,1]);
mu2(1,:) = Yi; mu2 = repmat(mu2,[n1,1]);
x = repmat(x, [1,n]);  y = repmat(y, [1,n]);
f = sum(normpdf(x,mu1,h1) .* normpdf(y,mu2,h2), 2) / n;

% _________________________________________________________________________
function f = ksdensity1d(x,Xi)
% Computes kernel density estimate in 1D (simplifiend version of ksdensity)
% x: points where the density function will be evaluated
% Xi: data

n = length(Xi); %sample size
nx = length(x);

% Choose bandwidths for Gaussian kernel
h = 1.06 * std(Xi)* n^(-0.2);

mu(1,:) = Xi; mu = repmat(mu,[nx,1]);
x = repmat(x, [1,n]);
f = sum(normpdf(x,mu,h), 2) / n;

% _________________________________________________________________________
function [C_ineq, C]=const_fun(WxWy,Wx_mat,Wy_mat)
% constraints for ABC analysis, px = length of Wx
global global_var;

px = global_var.px; py = global_var.py;

Wx = WxWy(1:px); Wy = WxWy(px+1:end);

%inequality constraints
C_ineq = [];

[p1, k] = size(Wx_mat);
[p2, k2] = size(Wy_mat);
if p1~=px || p2~=py, error('');end
if k~=k2, error('');end

C = zeros(1,2+2*k);
C(1) = Wx' * Wx -1;
C(2) = Wy' * Wy -1;

if k==0, return; end

C(3:k+2) = Wx_mat' * Wx;
C(k+3:2*k+2) = Wy_mat' * Wy;

% _________________________________________________________________________
function p_val = permutation_test(X,Y,Wx_mat,Wy_mat,a,b,R)
% The permutation test, 
% R = number of permutations
% Wx_mat = all canonical variates for X
% Wy_mat = all canonical variates for Y
global global_var;

verbose = global_var.verbose;

if verbose, fprintf('\n_______________\nPermutation Test\n\n'); end

% sample size
n = length(X(:,1));
% number of canonical pairs
p = length(Wx_mat(1,:));
p_val = zeros(1,p);

for i=1:p
    if verbose, fprintf('Coefficient %i/%i\n', i, p);end
    % the observed (negative of) ABC coefficient
    obs_D = ABC_coeff([Wx_mat(:,i); Wy_mat(:,i)],X,Y,a,b);
    % values of ABC coefficient for different permutation of X matrix
    D_vec = zeros(1,R);
    
    for r=1:R
        if verbose, fprintf('Permutation %i/%i\n', r, R);end
        if i==1
            WxWy = canon_variate(X(randperm(n),:),Y,[],[],a,b);
        else
            WxWy = canon_variate(X(randperm(n),:),Y,...
                Wx_mat(:,1:i-1),Wy_mat(:,1:i-1),a,b);
        end
        D_vec(r) = ABC_coeff(WxWy,X(randperm(n),:),Y,a,b);
    end
    % p-value of the test
    p_val(i) = mean(D_vec < obs_D);
    
end

% _________________________________________________________________________
function time_taken = sec2hour(t)
hour = fix(t/3600);
minute = fix((t-3600*hour)/60);
second = t - 3600*hour - 60*minute;
time_taken = '';
if hour ~=0, time_taken = [num2str(hour), ' hour(s), ']; end
if minute ~=0, time_taken = [time_taken, num2str(minute), ' minute(s) and ']; end
time_taken = [time_taken, num2str(second), ' second(s)'];

% _________________________________________________________________________
function [psi,phi]=ace(x)
% Alternating Conditional Expectation algorithm (ACE) to calculate optimal transformations
% by fast boxcar averaging of rank-ordered data.
% Program by: Henning U. Voss
% 
wl=5;   % width of smoothing kernel
oi=100; % maximum number of outer loop iterations
ii=10;  % maximum number of inner loop iterations
ocrit=10*eps; % numerical zeroes for convergence test
icrit=1e-4;
% shol=0; % 1-> show outer loop convergence, 0-> do not
% shil=0; % same for inner loop

[dim, ll] = size(x(2:end,:));

ind=zeros(dim+1,ll);
ranks=zeros(dim+1,ll);
for d=1:dim+1; [~,ind(d,:)]=sort(x(d,:)); end;
for d=1:dim+1; ranks(d,ind(d,:))=1:ll; end;
phi=(ranks-(ll-1)/2.)/ sqrt(ll*(ll-1)/12.);
ieps=1.; oeps=1.; oi1=1; ocrit1=1;
while oi1<=oi && ocrit1>ocrit
    ii1=1; icrit1=1;
    while ii1<=ii && icrit1>icrit
        for d=2:dim+1; sum0=0;
            for dd=2:dim+1; if dd ~=d; sum0=sum0+phi(dd,:); end; end;
            phi(d,:)=cef(phi(1,:)-sum0,ind(d,:),ranks(d,:),wl,ll);
        end;
        icrit1=ieps;
        if dim==1; sum0=phi(2,:); else sum0=sum(phi(2:dim+1,:)); end;
        ieps=sum((sum0-phi(1,:)).^2)/ll;
        icrit1=abs(icrit1-ieps);
%         if shil; disp(num2str([ii1 ieps icrit1])); end;
        ii1=ii1+1;
    end;
    phi(1,:)=cef(sum0,ind(1,:),ranks(1,:),wl,ll);
    phi(1,:)=(phi(1,:)-mean(phi(1,:)))/std(phi(1,:));
    ocrit1=oeps; oeps=sum((sum0-phi(1,:)).^2)/ll; ocrit1=abs(ocrit1-oeps);
%     if shol; disp(num2str([oi1 oeps ocrit1])); end;
    oi1=oi1+1;
end;
psi=corrcoef(phi(1,:),sum0); psi=psi(1,2);

% _________________________________________________________________________
function r=cef(y,ind,ranks,wl,ll)
cey=win(y(ind),wl,ll);
r=cey(ranks);

% _________________________________________________________________________
function r=win(y,wl,ll)
% wl=0,1,2...
r=conv(y,ones(2*wl+1,1));
r=r(wl+1:ll+wl)/(2*wl+1);
r(1:wl)=r(wl+1); r(ll-wl+1:ll)=r(ll-wl);
% _________________________________________________________________________
